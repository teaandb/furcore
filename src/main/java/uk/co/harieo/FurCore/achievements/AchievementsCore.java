package uk.co.harieo.FurCore.achievements;

import org.bukkit.Bukkit;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import uk.co.harieo.FurBridge.sql.FurDB;
import uk.co.harieo.FurBridge.sql.InfoCore;
import uk.co.harieo.FurBridge.sql.InfoTable;

public class AchievementsCore extends InfoCore {

	private Table<String, String, Integer> achievements = HashBasedTable
			.create(); // Stores the players achievement progress

	/**
	 * Checks whether an achievement has been completed
	 *
	 * @param achievable to check if is complete
	 * @return whether the achievement has been unlocked
	 */
	public boolean hasUnlockedAchievement(Achievable achievable) {
		// Makes sure the record exists and that the player has made maximum progress
		return achievements.contains(achievable.getId(), achievable.getGameName())
				&& achievements.get(achievable.getId(), achievable.getGameName()) >= achievable
				.getProgressMax();
	}

	/**
	 * Takes an array of {@link Achievable} and returns a list of the {@link Achievable} that have been unlocked
	 * according to {@link #hasUnlockedAchievement(Achievable)}
	 *
	 * @param achievables to check if they have been unlocked
	 * @return a list of the achievements that have been unlocked
	 */
	public List<Achievable> hasUnlockedAchievements(Achievable... achievables) {
		List<Achievable> list = new ArrayList<>();
		for (Achievable achievable : achievables) {
			if (hasUnlockedAchievement(achievable)) {
				list.add(achievable);
			}
		}
		return list;
	}

	/**
	 * Retrieves the amount of progress this player has made on the specified {@link Achievable}
	 *
	 * @param achievable to check the progress of
	 * @return the progress this player has made towards the achievement
	 */
	public int getProgressMade(Achievable achievable) {
		if (achievements.contains(achievable.getId(), achievable.getGameName())) {
			return achievements.get(achievable.getId(), achievable.getGameName());
		} else {
			return 0;
		}
	}

	/**
	 * Checks whether the given {@link Achievable} is stored in the cache {@link #achievements}. This will only be true
	 * if the specified {@link Achievable} was loaded from the database.
	 *
	 * @param achievable to check if stored
	 * @return whether the {@link Achievable} was stored
	 */
	private boolean isStored(Achievable achievable) {
		return achievements.contains(achievable.getId(), achievable.getGameName());
	}

	/**
	 * Sets the amount of progress this player has made for the specified {@link Achievable}
	 *
	 * @param achievable to set the progress of
	 * @param newProgress to set the value to
	 * @return whether the update was successful
	 */
	public CompletableFuture<Boolean> setProgressMade(Achievable achievable, int newProgress) {
		return CompletableFuture.supplyAsync(() -> {
			String statementParameter;
			boolean stored = isStored(achievable);
			if (stored) { // Check if this is already in the database or not
				statementParameter = "UPDATE achievements SET progress=? WHERE player_id=? AND achievement_id=? AND game=?";
			} else {
				statementParameter = "INSERT INTO achievements VALUES (?,?,?,?,?)";
			}

			try (Connection connection = FurDB.getConnection();
					PreparedStatement statement = connection.prepareStatement(statementParameter)) {
				if (stored) {
					statement.setInt(1, newProgress);
					statement.setInt(2, getPlayerInfo().getPlayerId());
					statement.setString(3, achievable.getId());
					statement.setString(4, achievable.getGameName());
					achievements.remove(achievable.getId(), achievable.getGameName());
				} else {
					statement.setInt(1, getPlayerInfo().getPlayerId());
					statement.setString(2, achievable.getId());
					statement.setString(3, achievable.getGameName());
					statement.setInt(4, newProgress);
					statement.setInt(5, achievable.getProgressMax());
				}

				achievements.put(achievable.getId(), achievable.getGameName(), newProgress);
				statement.executeUpdate();
				return true;
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}
		});
	}

	@Override
	public List<InfoTable> getReferencedTables() {
		return Collections.singletonList(
				InfoTable.get("achievements",
						"player_id int, achievement_id varchar(128), game varchar(128), progress int, progressMax int"));
	}

	@Override
	protected void load() {
		try (Connection connection = FurDB.getConnection();
				PreparedStatement statement =
						connection.prepareStatement(
								"SELECT achievement_id,game,progress FROM achievements WHERE player_id=?")) {
			statement.setInt(1, getPlayerInfo().getPlayerId());
			ResultSet result = statement.executeQuery();

			while (result.next()) {
				achievements.put(result.getString(1), result.getString(2), result.getInt(3));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
