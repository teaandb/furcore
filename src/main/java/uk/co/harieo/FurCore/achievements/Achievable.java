package uk.co.harieo.FurCore.achievements;

public interface Achievable {

	String getName();

	String getDescription();

	String getGameName();

	String getId();

	int getProgressMax();

}
