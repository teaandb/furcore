package uk.co.harieo.FurCore.items;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;
import uk.co.harieo.FurCore.FurCore;

public abstract class InteractiveItem implements Listener {

	public static final String RIGHT_CLICK_SUFFIX = ChatColor.GRAY + " " + FurCore.ARROWS + " Right Click";

	private ItemStack item;

	public InteractiveItem(Material material) {
		this.item = new ItemStack(material);
		Bukkit.getPluginManager().registerEvents(this, FurCore.getInstance());
	}

	public void setName(String newName) {
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(newName);
		item.setItemMeta(meta);
	}

	public void setLore(List<String> lore) {
		ItemMeta meta = item.getItemMeta();
		meta.setLore(lore);
		item.setItemMeta(meta);
	}

	public ItemStack getItem() {
		return item;
	}

	@EventHandler
	public void onItemDrop(PlayerDropItemEvent event) {
		if (event.getItemDrop().getItemStack().isSimilar(item)) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		if (event.getItemInHand().isSimilar(item)) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onClick(PlayerInteractEvent event) {
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) {
			if (event.getItem() != null && event.getItem().isSimilar(item)) {
				onRightClick(event);
			}
		}
	}

	public abstract void onRightClick(PlayerInteractEvent event);

}
