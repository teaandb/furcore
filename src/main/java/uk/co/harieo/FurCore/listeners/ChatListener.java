package uk.co.harieo.FurCore.listeners;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import uk.co.harieo.FurCore.FurCore;

public class ChatListener implements Listener {

	@EventHandler
	public void onAsyncChat(AsyncPlayerChatEvent event) {
		event.setFormat(
				event.getPlayer().getDisplayName() + " " + ChatColor.DARK_GRAY + FurCore.ARROWS + " " + ChatColor.WHITE
						+ event.getMessage());
	}

}
