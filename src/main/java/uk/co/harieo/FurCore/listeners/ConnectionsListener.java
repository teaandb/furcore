package uk.co.harieo.FurCore.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

import uk.co.harieo.FurBridge.players.PlayerInfo;

public class ConnectionsListener implements Listener {

	@EventHandler
	public void onPlayerLogin(PlayerLoginEvent event) {
		Player player = event.getPlayer();
		// Loads player info early to prevent any creation clashes
		PlayerInfo.loadPlayerInfo(player.getName(), player.getUniqueId());
	}

}
