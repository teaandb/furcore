package uk.co.harieo.FurCore.guis;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

public class GUIListener implements Listener {

	@EventHandler
	public void onClick(InventoryClickEvent event) {
		GUI gui = GUI.getGUI(event.getInventory());
		if (gui != null) {
			event.setCancelled(true);
			if (event.getWhoClicked().getOpenInventory() != null
					&& event.getWhoClicked().getOpenInventory().getTopInventory() != null
					&& event.getClickedInventory() != null
					&& event.getClickedInventory().equals(event.getWhoClicked().getOpenInventory().getTopInventory())) {
				gui.onRawClick(event);
			}
		}
	}

	@EventHandler
	public void onClose(InventoryCloseEvent event) {
		GUI gui = GUI.getGUI(event.getInventory());
		if (gui != null) {
			gui.onRawClose(event);
		}
	}

}
