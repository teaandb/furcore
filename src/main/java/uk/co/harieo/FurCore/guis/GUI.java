package uk.co.harieo.FurCore.guis;

import org.bukkit.Bukkit;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;

public abstract class GUI {

	private static Map<Inventory, GUI> CACHE = new HashMap<>();

	private Inventory inventory;
	private Map<Integer, ItemStack> items;
	private boolean registered = true;

	public GUI(String name, int rows) {
		int size = rows * 9;
		inventory = Bukkit.createInventory(null, size, name);
		items = new HashMap<>(size);
		CACHE.put(inventory, this);
	}

	/**
	 * Sets an item in the inventory
	 *
	 * @param slot the item should go in
	 * @param item to display
	 */
	public void setItem(int slot, ItemStack item) {
		registerCheck();
		items.put(slot, item);
		inventory.setItem(slot, item);
	}

	/**
	 * Gets the item displayed in a specified slot
	 *
	 * @param slot of the item
	 * @return the item or null if slot is empty
	 */
	public ItemStack getItem(int slot) {
		registerCheck();
		return items.get(slot);
	}

	/**
	 * @return the Bukkit version of {@link Inventory}
	 */
	public Inventory getInventory() {
		registerCheck();
		return inventory;
	}

	/**
	 * @return whether this GUI is still registered
	 */
	public boolean isRegistered() {
		return registered;
	}

	/**
	 * Clears all data from this instance to save memory
	 */
	public void unregister() {
		CACHE.remove(inventory);
		items.clear();
		inventory.clear();
		registered = false;
	}

	/**
	 * Checks whether {@link #unregister()} has been called and throws an exception if it has
	 */
	private void registerCheck() {
		if (!registered) {
			throw new IllegalStateException("Attempting to use unregistered GUI");
		}
	}

	/**
	 * A method with a declared body to prevent {@link AbstractMethodError} on calling {@link
	 * #onClick(InventoryClickEvent)}
	 *
	 * @param event the click event to be passed on to abstract methods
	 */
	void onRawClick(InventoryClickEvent event) {
		onClick(event);
	}

	public abstract void onClick(InventoryClickEvent event);

	/**
	 * A method with a declared body to prevent {@link AbstractMethodError} on calling {@link
	 * #onClose(InventoryCloseEvent)}
	 *
	 * @param event the click event to be passed on to abstract methods
	 */
	void onRawClose(InventoryCloseEvent event) {
		onClose(event);
	}

	public abstract void onClose(InventoryCloseEvent event);

	static GUI getGUI(Inventory inventory) {
		return CACHE.get(inventory);
	}

}
