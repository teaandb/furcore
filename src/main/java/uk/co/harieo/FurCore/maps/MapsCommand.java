package uk.co.harieo.FurCore.maps;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import app.ashcon.intake.Command;
import app.ashcon.intake.bukkit.parametric.annotation.Sender;
import app.ashcon.intake.group.At;
import app.ashcon.intake.group.Group;
import app.ashcon.intake.parametric.annotation.Text;
import java.nio.file.FileAlreadyExistsException;
import java.util.HashMap;
import java.util.Map;
import uk.co.harieo.FurBridge.rank.Rank;
import uk.co.harieo.FurCore.FurCore;
import uk.co.harieo.FurCore.ranks.RankCache;

public class MapsCommand implements Listener {

	private static final String PREFIX =
			ChatColor.AQUA + ChatColor.BOLD.toString() + "Maps " + ChatColor.DARK_GRAY + FurCore.ARROWS
					+ ChatColor.WHITE + " ";

	private Map<Player, String> plotters = new HashMap<>();

	@Group(@At("maps"))
	@Command(aliases = {"plot", "point"},
			 desc = "Registers a location on the map")
	public void plot(@Sender Player sender, String locationId) {
		if (RankCache.getCachedInfo(sender).hasPermission(Rank.MODERATOR)) {
			if (plotters.containsKey(sender)) {
				plotters.replace(sender, locationId);
			} else {
				plotters.put(sender, locationId);
			}
			sender.sendMessage(PREFIX + "You are now plotting points for " + ChatColor.GREEN + locationId);
		} else {
			sender.sendMessage(PREFIX + ChatColor.RED + "You do not have permission to do that!");
		}
	}

	@Group(@At("maps"))
	@Command(aliases = {"abort", "cancel"},
			 desc = "Stop plotting locations")
	public void cancel(@Sender Player sender) {
		if (plotters.containsKey(sender)) { // To prevent sending a message when it isn't necessary, looks nicer
			plotters.remove(sender);
			sender.sendMessage(PREFIX + "You are no longer plotting locations");
		}
	}

	@Group(@At("maps"))
	@Command(aliases = "name",
			 desc = "Edit the map settings")
	public void name(@Sender Player sender, @Text String value) {
		if (RankCache.getCachedInfo(sender).hasPermission(Rank.MODERATOR)) {
			MapImpl map = MapImpl.get(sender.getWorld());
			map.setFullName(value);
			sender.sendMessage(PREFIX + "Set this map's name to " + ChatColor.GREEN + value);
		} else {
			sender.sendMessage(PREFIX + ChatColor.RED + "You do not have permission to do that!");
		}
	}

	@Group(@At("maps"))
	@Command(aliases = {"author", "authors"},
			 desc = "Adds or removes an author from the map")
	public void author(@Sender Player sender, String requestType, String author) {
		if (RankCache.getCachedInfo(sender).hasPermission(Rank.MODERATOR)) {
			MapImpl map = MapImpl.get(sender.getWorld());
			if (requestType.equalsIgnoreCase("add")) {
				if (map.getAuthors().contains(author)) {
					sender.sendMessage(PREFIX + ChatColor.RED + "This author is already listed!");
				} else {
					map.addAuthor(author);
					sender.sendMessage(PREFIX + "You have listed " + ChatColor.GREEN + author + ChatColor.WHITE
							+ " as a map author!");
				}
			} else if (requestType.equalsIgnoreCase("remove")) {
				if (!map.getAuthors().contains(author)) {
					sender.sendMessage(PREFIX + ChatColor.RED
							+ "There is no author listed with that name (case-sensitive), use '/maps authors list' to check!");
				} else {
					map.removeAuthor(author);
					sender.sendMessage(PREFIX + "You have unlisted " + ChatColor.GREEN + author + ChatColor.WHITE
							+ " as a map author!");
				}
			} else if (requestType.equalsIgnoreCase("list")) {
				sender.sendMessage(PREFIX + ChatColor.YELLOW + "List of Map Authors");
				for (String a : map.getAuthors()) {
					sender.sendMessage(ChatColor.GREEN + a);
				}
			} else {
				sender.sendMessage(PREFIX + ChatColor.RED + "Unknown Argument: " + requestType);
				sender.sendMessage(PREFIX + "Expected: [add,remove,list]");
			}
		} else {
			sender.sendMessage(PREFIX + ChatColor.RED + "You do not have permission to do that!");
		}
	}

	@Group(@At("maps"))
	@Command(aliases = "commit",
			 desc = "Saves all map data to a JSON file in the map directory")
	public void commit(@Sender Player sender) {
		if (RankCache.getCachedInfo(sender).hasPermission(Rank.MODERATOR)) {
			MapImpl map = MapImpl.get(sender.getWorld());
			if (map.isValid()) {
				try {
					if (map.commitToFile()) {
						sender.sendMessage(PREFIX + ChatColor.GREEN + "Successfully " + ChatColor.WHITE
								+ "committed data to map.json in map directory!");
					} else {
						sender.sendMessage(PREFIX + ChatColor.RED + "An error occurred committing the file");
					}
				} catch (FileAlreadyExistsException ignored) {
					sender.sendMessage(PREFIX + ChatColor.RED
							+ "A committed file already exists for this world and cannot be overwritten");
				}
			} else {
				sender.sendMessage(PREFIX + ChatColor.RED + "Prerequisites for Commit");
				sender.sendMessage(PREFIX + (map.isLocationPlotted("spawn") ? ChatColor.GREEN : ChatColor.RED)
						+ "Must have location 'spawn'");
				sender.sendMessage(PREFIX + (map.getFullName() != null ? ChatColor.GREEN : ChatColor.RED)
						+ "Must have a map name (/maps name [name])");
				sender.sendMessage(PREFIX + (!map.getAuthors().isEmpty() ? ChatColor.GREEN : ChatColor.RED)
						+ "Must have at least 1 author (/maps author [type] [name])");
			}
		} else {
			sender.sendMessage(PREFIX + ChatColor.RED + "You do not have permission to do that!");
		}
	}

	@Group(@At("maps"))
	@Command(aliases = "clear",
			 desc = "Clears all locations stored")
	public void clear(@Sender Player sender) {
		if (RankCache.getCachedInfo(sender).hasPermission(Rank.MODERATOR)) {
			MapImpl.get(sender.getWorld()).clearLocations();
			sender.sendMessage(
					PREFIX + "You have " + ChatColor.RED + "cleared " + ChatColor.WHITE + "all plotted locations!");
		} else {
			sender.sendMessage(PREFIX + ChatColor.RED + "You do not have permission to do that!");
		}
	}

	@Group(@At("maps"))
	@Command(aliases = {"list", "locations"},
			 desc = "List all plotted locations")
	public void list(@Sender Player sender) {
		if (RankCache.getCachedInfo(sender).hasPermission(Rank.MODERATOR)) {
			Map<Location, String> map = MapImpl.get(sender.getWorld()).getAllLocations();
			for (Location location : map.keySet()) {
				String id = map.get(location);
				sender.sendMessage(ChatColor.GREEN + id + ChatColor.WHITE + " at " + formCoordinates(location));
			}
		} else {
			sender.sendMessage(PREFIX + ChatColor.RED + "You do not have permission to do that!");
		}
	}

	@EventHandler
	public void onRightClick(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		if (plotters.containsKey(player) && event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			event.setCancelled(true);
			Location location = event.getClickedBlock().getLocation().clone();
			MapImpl map = MapImpl.get(player.getWorld());

			String locationId = plotters.get(player);

			if (locationId.equalsIgnoreCase("remove")) {
				if (!map.isLocationPlotted(location)) {
					player.sendMessage(PREFIX + ChatColor.RED
							+ "That location has not been plotted yet (and you are using 'remove')");
				} else {
					map.removeLocation(location);

					Block block = location.getBlock();
					if (block.getState() instanceof Sign) {
						block.setType(Material.AIR); // Removes the marker sign if it exists still
					}

					player.sendMessage(
							PREFIX + "Successfully deleted plotted location at " + ChatColor.YELLOW + formCoordinates(
									location));
				}
			} else {
				if (map.isLocationPlotted(location)) { // Make sure the location won't overwrite anything
					player.sendMessage(PREFIX + ChatColor.RED + "That location has already been plotted!");
				} else {
					map.addLocation(locationId, location);
					player.sendMessage(
							PREFIX + "Plotted location " + ChatColor.YELLOW + formCoordinates(location)
									+ ChatColor.WHITE + " as "
									+ ChatColor.GREEN + locationId);
				}
			}
		}
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		plotters.remove(event.getPlayer());
	}

	private String formCoordinates(Location location) {
		return "(" + location.getBlockX() + "," + location.getBlockY() + "," + location.getBlockZ() + ")";
	}

}
