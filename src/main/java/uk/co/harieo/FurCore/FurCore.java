package uk.co.harieo.FurCore;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import app.ashcon.intake.Intake;
import app.ashcon.intake.bukkit.BukkitIntake;
import app.ashcon.intake.parametric.Injector;
import java.util.function.Consumer;
import uk.co.harieo.FurBridge.sql.InfoTable;
import uk.co.harieo.FurCore.guis.GUIListener;
import uk.co.harieo.FurCore.intake.RankModule;
import uk.co.harieo.FurCore.listeners.ChatListener;
import uk.co.harieo.FurCore.listeners.ConnectionsListener;
import uk.co.harieo.FurCore.maps.MapImpl;
import uk.co.harieo.FurCore.maps.MapsCommand;
import uk.co.harieo.FurCore.ranks.RankCache;
import uk.co.harieo.FurCore.ranks.RankCommand;

public class FurCore extends JavaPlugin {

	public static final char ARROWS = '»';

	private static FurCore INSTANCE;
	private static MapImpl PRIMARY_WORLD;

	private Consumer<World> onWorldParseCompletion;

	public void onEnable() {
		INSTANCE = this;

		verifyEssentialTables();
		parsePrimaryWorld();

		MapsCommand mapsCommand = new MapsCommand();
		registerEvents(new RankCache(), new ChatListener(), mapsCommand, new GUIListener(), new ConnectionsListener());
		registerCommands(new RankCommand(), mapsCommand);
	}

	public void onDisable() {

	}

	/**
	 * @return the world that players spawn in
	 */
	public MapImpl getPrimaryWorld() {
		return PRIMARY_WORLD;
	}

	/**
	 * Parses "world" for a map.json using {@link MapImpl#parseWorld(World)}
	 */
	private void parsePrimaryWorld() {
		String worldName = "world"; // Stating it just makes it easier to change here
		getLogger().info("Parsing world as map");
		try {
			World world = Bukkit.getWorld(worldName);
			if (world != null) {
				PRIMARY_WORLD = MapImpl.parseWorld(world);
				if (PRIMARY_WORLD != null) {
					// Spawns can never be empty as a validity error would be thrown before this point
					PRIMARY_WORLD.getWorld().setSpawnLocation(PRIMARY_WORLD.getSpawns().get(0));
					getLogger().info("Successfully parsed " + worldName);
				} else {
					getLogger().warning("Failed to parse " + worldName + " for unknown reason");
				}
			} else {
				getLogger().warning("Couldn't find a world called '" + worldName + "'");
			}
		} catch (Exception e) { // This minimises impact on main thread
			e.printStackTrace();
			getLogger().warning("Failed to load map '" + worldName + "'");
		}
	}

	/**
	 * Attempts to create tables that are critical to every function in the system if they do not already exist
	 */
	private void verifyEssentialTables() {
		InfoTable usersTable = InfoTable.get("users",
				"id int primary key auto_increment, uuid varchar(128) unique key not null, name varchar(64) not null");
		usersTable.createTable();
	}

	/**
	 * Registers listeners in bulk
	 *
	 * @param listeners to be registered
	 */
	private void registerEvents(Listener... listeners) {
		for (Listener listener : listeners) {
			Bukkit.getPluginManager().registerEvents(listener, this);
		}
	}

	/**
	 * Registers commands to the Intake system in bulk and injects the necessary {@link
	 * app.ashcon.intake.parametric.Module} parameters
	 */
	public void registerCommands(Object... commands) {
		Injector injector = Intake.createInjector();
		// Bind modules
		injector.install(new RankModule());
		new BukkitIntake(this, injector, commands);
	}

	/**
	 * @return the instance of this class created on server start
	 */
	public static FurCore getInstance() {
		return INSTANCE;
	}

}
