package uk.co.harieo.FurCore.intake;

import app.ashcon.intake.argument.ArgumentException;
import app.ashcon.intake.argument.ArgumentParseException;
import app.ashcon.intake.argument.CommandArgs;
import app.ashcon.intake.argument.Namespace;
import app.ashcon.intake.parametric.AbstractModule;
import app.ashcon.intake.parametric.Provider;
import app.ashcon.intake.parametric.ProvisionException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nullable;
import uk.co.harieo.FurBridge.rank.Rank;

/**
 * The purpose of this module is to allow {@link Rank} to be used as a command argument in Intake
 */
public class RankModule extends AbstractModule {

	@Override
	public void configure() {
		bind(Rank.class).toProvider(new RankProvider());
	}

	private static class RankProvider implements Provider<Rank> {

		@Override
		public String getName() {
			return "rank";
		}

		@Nullable
		@Override
		public Rank get(CommandArgs args, List<? extends Annotation> mods)
				throws ArgumentException, ProvisionException {
			String rawRank = args.next();

			Rank rank = null;
			for (Rank r : Rank.values()) {
				if (r.name().equalsIgnoreCase(rawRank.toLowerCase())) {
					rank = r;
				}
			}

			if (rank == null) {
				throw new ArgumentParseException("No rank with the name " + rawRank + " was found!");
			}

			return rank;
		}

		@Override
		public List<String> getSuggestions(String prefix, Namespace namespace, List<? extends Annotation> modifiers) {
			List<String> rankNames = new ArrayList<>();
			for (Rank rank : Rank.values()) {
				if (rank.name().startsWith(prefix)) {
					rankNames.add(rank.name());
				}
			}
			return rankNames;
		}

	}

}
