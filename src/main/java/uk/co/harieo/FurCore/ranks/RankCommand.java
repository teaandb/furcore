package uk.co.harieo.FurCore.ranks;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import app.ashcon.intake.Command;
import app.ashcon.intake.bukkit.parametric.annotation.Sender;
import app.ashcon.intake.group.At;
import app.ashcon.intake.group.Group;
import javax.annotation.Nullable;
import uk.co.harieo.FurBridge.players.PlayerInfo;
import uk.co.harieo.FurBridge.rank.Rank;
import uk.co.harieo.FurBridge.rank.RankInfo;
import uk.co.harieo.FurBridge.sql.InfoCore;

public class RankCommand {

	@Group(@At("ranks"))
	@Command(aliases = {"", "help"},
			 desc = "Default rank command")
	public void rank(@Sender Player sender) {
		if (RankCache.getCachedInfo(sender).hasPermission(Rank.ADMINISTRATOR)) {
			sender.sendMessage(ChatColor.GREEN + "/rank give <player> <rank> " + ChatColor.WHITE
					+ "- Gives a player the specified rank");
			sender.sendMessage(ChatColor.GREEN + "/rank remove <player> <rank> " + ChatColor.WHITE
					+ "- Removes a rank from the specified player");
		} else {
			sender.sendMessage(ChatColor.RED + "I'm sorry sir, you require Administrator to use this command set!");
		}
	}

	@Group(@At("ranks"))
	@Command(aliases = "give",
			 desc = "Add a rank to a players rank list",
			 usage = "[player] [rank]")
	public void giveRank(CommandSender sender, String playerName, Rank rank) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (!RankCache.getCachedInfo(player).hasPermission(Rank.ADMINISTRATOR)) {
				sender.sendMessage(ChatColor.RED + "You do not have permission to do that!");
				return;
			}
		}

		Player player = Bukkit.getPlayer(playerName);
		if (player != null) {
			continueAddingRank(sender, RankCache.getCachedInfo(player), rank, player);
			return; // Saves database queries if we already have the rank info loaded
		}

		// Make sure the sender is an Administrator
		PlayerInfo.queryPlayerInfo(playerName).whenComplete((playerInfo, error) -> {
			if (error != null) {
				error.printStackTrace();
				sender.sendMessage(ChatColor.RED + "An error occurred querying that player's information");
			} else if (!playerInfo.wasSuccessfullyLoaded()) {
				sender.sendMessage(ChatColor.RED
						+ "Either that player has never logged in before or the system encountered an unknown error");
			} else {
				InfoCore.get(RankInfo.class, playerInfo).whenComplete(((rankInfo, error1) -> {
					if (error1 != null) {
						error1.printStackTrace();
						sender.sendMessage(ChatColor.RED + "An error occurred retrieving that player's rank");
					} else if (rankInfo.hasErrorOccurred()) {
						sender.sendMessage(
								ChatColor.RED + "An unknown error occurred retrieving that player's rank");
					} else if (rankInfo.getRanks().contains(rank)) {
						sender.sendMessage(ChatColor.RED + "That player already has that rank");
					} else {
						continueAddingRank(sender, rankInfo, rank, null);
					}
				}));
			}
		});
	}

	@Group(@At("ranks"))
	@Command(aliases = "remove",
			 desc = "Remove a rank from a players rank list",
			 usage = "[player] [rank]")
	public void removeRank(CommandSender sender, String playerName, Rank rank) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (!RankCache.getCachedInfo(player).hasPermission(Rank.ADMINISTRATOR)) {
				sender.sendMessage(ChatColor.RED + "You do not have permission to do that!");
				return;
			}
		}

		Player player = Bukkit.getPlayer(playerName);
		if (player != null) {
			continueRemovingRank(sender, RankCache.getCachedInfo(player), rank, player);
			return; // Saves database queries if we already have the rank info loaded
		}

		PlayerInfo.queryPlayerInfo(playerName).whenComplete((playerInfo, error) -> {
			if (error != null) {
				error.printStackTrace();
				sender.sendMessage(ChatColor.RED + "An error occurred querying that player's information");
			} else if (!playerInfo.wasSuccessfullyLoaded()) {
				sender.sendMessage(ChatColor.RED
						+ "Either that player has never logged in before or the system encountered an unknown error");
			} else {
				InfoCore.get(RankInfo.class, playerInfo).whenComplete(((rankInfo, error1) -> {
					if (error1 != null) {
						error1.printStackTrace();
						sender.sendMessage(ChatColor.RED + "An error occurred retrieving that player's rank");
					} else if (rankInfo.hasErrorOccurred()) {
						sender.sendMessage(
								ChatColor.RED + "An unknown error occurred retrieving that player's rank");
					} else if (!rankInfo.getRanks().contains(rank)) {
						sender.sendMessage(ChatColor.RED + "That player doesn't have that rank");
					} else {
						continueRemovingRank(sender, rankInfo, rank, null);
					}
				}));
			}
		});
	}

	/**
	 * Finishes the final stage of adding the specified rank to the specified player
	 *
	 * @param sender who is giving the rank
	 * @param rankInfo of the player the rank is being given to
	 * @param toAdd the rank that is being given
	 * @param target instance of {@link Player} the rank is being given to, which can be null if does not apply
	 */
	private void continueAddingRank(CommandSender sender, RankInfo rankInfo, Rank toAdd, @Nullable Player target) {
		rankInfo.addRank(toAdd).whenComplete((success, error2) -> {
			if (error2 != null) {
				error2.printStackTrace();
				sender.sendMessage(
						ChatColor.RED + "An error occurred adding that rank to the database");
			} else if (!success) {
				sender.sendMessage(ChatColor.RED
						+ "An unknown error occurred adding that rank to the database");
			} else {
				sender.sendMessage(
						ChatColor.GREEN + "Successfully dispatched that rank!");
				if (target != null) {
					RankCache.reloadInfo(target); // This will set their tab and chat prefixes as well
				}
			}
		});
	}

	/**
	 * Finishes the final stage of removing the specified rank from the specified player
	 *
	 * @param sender who is removing the rank
	 * @param rankInfo of the player the rank is being removed from
	 * @param toRemove the rank that is being removed
	 * @param target instance of {@link Player} the rank is being removed from, which can be null if does not apply
	 */
	private void continueRemovingRank(CommandSender sender, RankInfo rankInfo, Rank toRemove, @Nullable Player target) {
		rankInfo.removeRank(toRemove).whenComplete((success, error2) -> {
			if (error2 != null) {
				error2.printStackTrace();
				sender.sendMessage(
						ChatColor.RED + "An error occurred updating that rank in the database");
			} else if (!success) {
				sender.sendMessage(ChatColor.RED
						+ "An unknown error occurred updating that rank in the database");
			} else {
				sender.sendMessage(
						ChatColor.GREEN + "Successfully removed that rank!");
				if (target != null) {
					RankCache.reloadInfo(target); // This will set their tab and chat prefixes as well
				}
			}
		});
	}

}
