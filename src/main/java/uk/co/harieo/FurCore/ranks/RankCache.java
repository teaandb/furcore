package uk.co.harieo.FurCore.ranks;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.Map;
import uk.co.harieo.FurBridge.rank.Rank;
import uk.co.harieo.FurBridge.rank.RankInfo;
import uk.co.harieo.FurBridge.sql.InfoCore;
import uk.co.harieo.FurCore.FurCore;

public class RankCache implements Listener {

	private static Map<Player, RankInfo> CACHE = new HashMap<>();

	/**
	 * Gets the instance of {@link RankInfo} cached on player join so that using asynchronous threads isn't necessary
	 * for permissible functions
	 *
	 * @param player to get the cached rank information for
	 * @return the cached instance of {@link RankInfo}
	 */
	public static RankInfo getCachedInfo(Player player) {
		if (!CACHE.containsKey(player)) {
			CACHE.put(player, RankInfo.getUnloadedInfo()); // This helps prevent unnecessary system interruption
			FurCore.getInstance().getLogger().warning("Player " + player.getName() + " had no cached RankInfo, which is an error");
		}
		return CACHE.get(player);
	}

	/**
	 * Removes and re-retrieves the specified player's rank information
	 *
	 * @param player to reload rank information of
	 */
	static void reloadInfo(Player player) {
		CACHE.remove(player);
		loadInfo(player);
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		loadInfo(event.getPlayer());
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		CACHE.remove(event.getPlayer());
	}

	/**
	 * Loads player information from the database and adds it to the cache
	 *
	 * @param player to load information for
	 */
	private static void loadInfo(Player player) {
		InfoCore.get(RankInfo.class, player.getUniqueId()).whenComplete((rankInfo, error1) -> {
			if (error1 != null) {
				error1.printStackTrace();
				CACHE.put(player, RankInfo.getUnloadedInfo()); // Null is still bad
				return;
			}

			Rank primaryRank = rankInfo.getPrimaryRank();
			if (primaryRank.hasPrefix()) {
				String displayName = primaryRank.getPrefix() + ChatColor.WHITE + player.getName();
				player.setPlayerListName(displayName);
				player.setDisplayName(displayName);
			}
			CACHE.put(player, rankInfo);
		});
	}

}
